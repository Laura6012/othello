﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace othello
{
    /// <summary>
    /// Classe Joueur.
    /// </summary>
    public class Joueur
    {
        #region Attributs
        /// <summary>
        /// Atributs de la classe Joueur.
        /// </summary>
        private string m_strNom;
        private byte m_nbJetons;
        private int m_couleur;
        #endregion

        #region Accesseurs
        /// <summary>
        /// Accesseur Couleur en lecture et écriture.
        /// </summary>
        public int Couleur
        {
            get { return m_couleur; }
            set { if (value == Tableau.NOIR || value == Tableau.BLANC) m_couleur = value; }
        }

        /// <summary>
        /// Accesseur Nom en lecture et écriture.
        /// </summary>
        public string Nom
        {
            get { return m_strNom; }

            //Seul le constructeur change le nom pour s'assurer
            //qu'il n'y ai pas d'erreurs
            private set { m_strNom = value; }
        }

        /// <summary>
        /// Accesseur NbJeton en lecture et écriture.
        /// </summary>
        public byte NbJetons
        {
            get { return m_nbJetons; }
            set
            {
                if(value >= 0 && value <= 64)
                {
                    m_nbJetons = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Le nombre des jetons est" +
                        "en dehors de la plage permise.");
                }
            }
        }
        #endregion

        #region Constructeur
        /// <summary>
        /// Constructeur Joueur avec paramètres.
        /// </summary>
        /// <param name="nom">Valeur Nom à fournir.</param>
        /// <param name="couleur">Valeur Couleur à fournir.</param>
        public Joueur(string nom, int couleur)
        {
            this.Nom = nom;
            this.Couleur = couleur;
            this.NbJetons = 2;            
        }
        #endregion

        /// <summary>
        /// Méthode qui retourne le nom de la personne ainsi que le nombre
        /// de jeton de sa couleur sous forme de chaîne de caractère.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
           
            //Ajouter le nombre de coups joués et le temps de jeu du joueur.
            return "Nom: " + this.m_strNom + " ---- Nombre de jetons: " + this.m_nbJetons;
        }
    }
}
