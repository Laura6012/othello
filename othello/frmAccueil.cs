﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace othello
{
    public partial class frmAccueil : Form
    {
        //Constantes qui représentent les couleurs ===================================================
        public const int NOIR = -1;
        public const int BLANC = 1;
        public const int VIDE = 0;

        private NiveauDifficulte difficulte;

        public NiveauDifficulte Difficulte
        {
            get { return difficulte; }
            set { difficulte = value; }
        }


        public frmAccueil()
        {
            InitializeComponent();
        }
        private void FrmAccueil_Load(object sender, EventArgs e)
        {
            //Par défaut, leJoueur = noir et Adversaire = blanc
            cboStylePions.SelectedIndex = 1;
            cboDifficulte.SelectedIndex = 0;
            cboMode.SelectedIndex = 1;
            txtCouleurAdverse.Text = "Blanc";
            pboImageJoueur.ImageLocation = "C:\\images\\noir.jpg";
            pboImageAdversaire.ImageLocation = "C:\\images\\blanc.jpg";
            txtNomAdversaire.Text = "no name";
            radJoueurCommence.Checked = true;
        }

        private void btnSelectionnerImgJoueur_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = "Png|*.png|Jpg|*.jpg|Gif|*.gif";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                this.pboImageJoueur.Image = new Bitmap(ofd.FileName);
                this.pboImageJoueur.ImageLocation = ofd.FileName;

            }
        }

        private void btnSelectionnerImgAdv_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = "Png|*.png|Jpg|*.jpg|Gif|*.gif";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                this.pboImageAdversaire.Image = new Bitmap(ofd.FileName);
                this.pboImageAdversaire.ImageLocation = ofd.FileName;

            }
        }
        

       
        private void CboMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboMode.Text == "Joueur contre Joueur")
            {
                cboDifficulte.Visible = false;
                cboDifficulte.Enabled = false;
            }
            else
            {
                cboDifficulte.Visible = true;
                cboDifficulte.Enabled = true;
            }
        }

        private void BtnCommencerPartie_Click(object sender, EventArgs e)
        {
            if (txtNomJoueur.Text.Trim() == "")
            {
                MessageBox.Show("Vous devez entrer votre nom!", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (pboImageJoueur.ImageLocation == pboImageAdversaire.ImageLocation)
            {
                MessageBox.Show("Les deux joueurs ne peuvent pas avoir la même image", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }           
            else
            {
                int couleur;
                switch(cboStylePions.SelectedIndex)
                {
                    case 0:
                        couleur = BLANC;
                        break;
                    case 1:
                        couleur = NOIR;
                        break;
                        //Par défaut, NOIR
                    default:
                        couleur = NOIR;
                        break;
                }
                //Création du formulaire frmJeu
                frmJeu frmjeu1 = new frmJeu();

                //Création des joueurs
                frmjeu1.leJoueur = new Joueur(txtNomJoueur.Text.Trim(), couleur);

                if (cboMode.Text == "Joueur contre Joueur")
                {
                    frmjeu1.JoueurContreJouer = true;
                    frmjeu1.leJoueurAdverse = new Joueur(txtNomAdversaire.Text.Trim(), couleur * -1);
                }
                else
                {
                    IA_Othello othello = new IA_Othello(frmjeu1.LeTableau, Difficulte, couleur * -1);
                    frmjeu1.leJoueurAdverse = othello.JoueurIA;
                    frmjeu1.AI = othello; 

                }
                frmjeu1.CheminImageJoueur = this.pboImageJoueur.ImageLocation;
                frmjeu1.CheminImageAdversaire = this.pboImageAdversaire.ImageLocation;


                if (radJoueurCommence.Checked == true)
                    frmjeu1.JoueurCommence = true;
                else
                    frmjeu1.JoueurCommence = false;

                frmjeu1.Show();
            }
        }

        //TODO :À effacer?
        private void Button1_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cboStylePions_SelectedIndexChanged(object sender, EventArgs e)
        {
            string tempo;
            switch (cboStylePions.SelectedIndex)
            {
                case 0:
                    //Le joueur est BLANC donc...
                    txtCouleurAdverse.Text = "Noir";
                    tempo = this.pboImageJoueur.ImageLocation;
                    this.pboImageJoueur.ImageLocation = this.pboImageAdversaire.ImageLocation;
                    this.pboImageAdversaire.ImageLocation = tempo;
                    break;
                case 1:
                    //Le joueur est NOIR donc...
                    txtCouleurAdverse.Text = "Blanc";
                    tempo = this.pboImageJoueur.ImageLocation;
                    this.pboImageJoueur.ImageLocation = this.pboImageAdversaire.ImageLocation;
                    this.pboImageAdversaire.ImageLocation = tempo;
                    break;
                //Par défaut, le joueur est NOIR donc...
                default:
                    txtCouleurAdverse.Text = "Blanc";
                    break;
            }
        }
        private void cboDifficulte_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cboDifficulte.Text)
            {
                case "Facile":
                    Difficulte = NiveauDifficulte.Facile;
                    break;
                case "Normal":
                    Difficulte = NiveauDifficulte.Normal;
                    break;
                case "Difficile":
                    Difficulte = NiveauDifficulte.Difficile;
                    break;
                case "Professionnel":
                    Difficulte = NiveauDifficulte.Professionnel;
                    break;
            }


        }

    }
}
