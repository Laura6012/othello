﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace othello
{
    /// <summary>
    /// Classe Tableau qui représente la table de jeu de l'application.
    /// </summary>
    public class Tableau
    {
        #region CONSTANTES
        /// <summary>
        /// Constantes de la classe Tableau qui représentent les couleurs.
        /// </summary>
        public const int NOIR = -1;
        public const int BLANC = 1;
        public const int VIDE = 0;
        public const int COLONNES = 8;
        public const int LIGNES = 8;

        //NOTE : Ligne = y (déplacement à la vertical) et Colonne = x (déplacement à l'horizontale)
        //Le premier indice dans [y,x] est la ligne et le deuxième est la colonne.

        #endregion

        public enum Direction { TopLeft, Top,TopRight,Right,BottomRight,Bottom,BottomLeft,Left }
        
        #region ATTRIBUTS
        /// <summary>
        /// Attributs de la classe Tableau.
        /// </summary>
        private int[,] m_tableau;
        private Coup[,] m_coups;
        private bool[,] m_placementPossible;

        #endregion

        #region ACCESSEURS

        public bool[,] PlacementPossible
        {
            get { return m_placementPossible; }
            set { m_placementPossible = value; }
        }

        /*Accesseur NbCases en lecture permettant d'obtenir le length du plateau
         (afin de pouvoir faire this.NbCases au lieu de this.m_tableau.length)*/
        public int NbCases
        {
            get { return this.m_tableau.Length; }
        }

        /*Indexeur permettant d'obtenir/modifier le contenu d'une case du plateau
         (afin de pouvoir faire this[y,x] au lieu de this.m_tableau[y,x])
         Retourne une valeur int*/
        /// <summary>
        /// Tableau de jetons du jeu
        /// </summary>
        /// <returns>Le type de pion à l'emplacement</returns>
        public int this[int y, int x]
        {
            get
            {
                if (y < 0 || y >= LIGNES || x < 0 || x >= COLONNES)
                    throw new ArgumentOutOfRangeException("Les indices doivent être supérieurs ou égaux à 0 et inférieurs ou égaux à 7");

                return this.m_tableau[y, x];
            }
            set
            {
                if (y < 0 || y >= LIGNES || x < 0 || x >= COLONNES)
                    throw new ArgumentOutOfRangeException("Les indices doivent être supérieurs ou égaux à 0 et inférieurs ou égaux à 7");

                this.m_tableau[y, x] = value;
            }
        }

        /// <summary>
        /// Accesseur NbJetonsNoirs permettant d'obtenir le nombre actuel de jetons noirs sur le plateau
        /// Le tableau est parcouru à chaque appel de l'accesseur.
        /// </summary>
        public int NbJetonsNoirs
        {
            get
            {
                int nbJetonsNoirs = 0;
                for (int y = 0; y < LIGNES; y++)
                {
                    for (int x = 0; x < COLONNES; x++)
                    {
                        if (this[y, x] == NOIR)
                            nbJetonsNoirs += 1;
                    }
                }
                return nbJetonsNoirs;
            }
        }

        /// <summary>
        /// Accesseur NbJetonsBlancs permettant d'obtenir le nombre actuel de jetons blancs
        /// sur le plateau Le tableau est parcouru à chaque appel de l'accesseur.
        /// </summary>
        public int NbJetonsBlancs
        {
            get
            {
                int nbJetonsBlancs = 0;
                for (int y = 0; y < LIGNES; y++)
                {
                    for (int x = 0; x < COLONNES; x++)
                    {
                        if (this[y, x] == BLANC)
                            nbJetonsBlancs += 1;
                    }
                }
                return nbJetonsBlancs;
            }
        }

        /// <summary>
        /// Accesseur permettant d'obtenir le nombre de jetons placés sur le plateau
        /// Note : si NbJetonsPlaces = 64, la partie est terminée, il n'y a plus de place sur le plateau.
        /// </summary>
        public int NbJetonsPlaces
        {
            get
            { return this.NbJetonsBlancs + this.NbJetonsNoirs; }
        }

        //Permet d'obtenir le nombre de ligne (y) du tableau
        public int NbLignes
        { get { return this.m_tableau.GetLength(1); } }

        //Permet d'obtenir le nombre de colonnes (x) du tableau
        public int NbColonnes
        { get { return this.m_tableau.GetLength(0); } }

        #endregion

        #region CONSTRUCTEURS

        /// <summary>
        /// Constructeur par défaut, crée un tableau vide
        /// </summary>
        public Tableau()
        {
            m_tableau = new int[LIGNES, COLONNES];
            m_coups = new Coup[LIGNES, COLONNES];
            m_placementPossible = new bool[LIGNES, COLONNES];
        }

        //Constructeur Copie qui creer un nouveau tableau et copie le tableau recu en parametre de dans 
        public Tableau(Tableau TabASimule)
        {
            this.m_tableau = new int[LIGNES, COLONNES];
            this.m_coups = new Coup[LIGNES, COLONNES];
            this.m_placementPossible = new bool[LIGNES, COLONNES];

            for (int i = 0; i < LIGNES; i++) //Y
            {
                for (int j = 0; j < COLONNES; j++) //X
                {
                    this.m_tableau[i, j] = TabASimule.m_tableau[i, j];
                    this.m_coups[i, j] = TabASimule.m_coups[i, j];
                    this.PlacementPossible[i, j] = TabASimule.m_placementPossible[i, j];
                }
            }

            int t = 0;
        }
        #endregion

        #region MÉTHODES UTILITAIRES
        /// <summary>
        /// Réinitialise le tableau en VIDE
        /// </summary>
        public void TableauVide()
        {
            for (int i = 0; i < LIGNES; i++) //Y
            {
                for (int j = 0; j < COLONNES; j++) //X
                {
                    m_tableau[i, j] = VIDE;
                }
            }
        }

        /// <summary>
        /// Méthode qui valide si la couleur fournie est valide
        /// </summary>
        /// <param name="couleurPlace"></param>
        /// <returns></returns>
        private void CouleurValide(int couleurPlace)
        {
            if (couleurPlace != NOIR && couleurPlace != BLANC)
                throw new ArgumentException("Veuillez choisir une couleur valide.");
        }

        /// <summary>
        /// Méthode qui valide si les positions fournis sont valides
        /// </summary>
        /// <param name="posX"></param>
        /// <param name="posY"></param>
        private void CoordonneesValides(int posY, int posX)
        {
            if (posX < 0 && posY < 0 && posX >= COLONNES && posY >= LIGNES)
                throw new ArgumentOutOfRangeException("Veuillez entrer une coodonnée valide");
        }
        #endregion

        /// <summary>
        /// Réinitialise le tableau de jeu pour une nouvelle partie
        /// </summary>
        public void NouvellePartie(int couleurDuJoueur)
        {
            TableauVide();

            if (couleurDuJoueur == BLANC)
            {
                this[3, 3] = NOIR;
                this[4, 4] = NOIR;
                this[3, 4] = BLANC;
                this[4, 3] = BLANC;
            }
            else
            {
                this[3, 3] = BLANC;
                this[4, 4] = BLANC;
                this[3, 4] = NOIR;
                this[4, 3] = NOIR;
            }
        }

        /// <summary>
        /// Mets a jour le tableau de bool qui indique les positions ou il est possible de jouer.
        /// Retourne faux si il n'y a pas de place possible
        /// </summary>
        /// <param name="couleurPlacee">Couleur qui sera placé lors du cours</param>
        public bool NouveauTour(int couleurPlacee)
        {
            //Reset du tableau de coups
            for (int i = 0; i < LIGNES; i++)
            {
                for (int j = 0; j < COLONNES; j++)
                {
                    PlacementPossible[i, j] = false;
                    m_coups[i, j] = new Coup();
                }
            }

            for (int i = 0; i < LIGNES; i++)
            {
                for (int j = 0; j < COLONNES; j++)
                {
                    PlacementPossible[i, j] = MouvementLegal(i, j, couleurPlacee);
                }
            }

            //Vérification qu'il y a au moins un coup qui est possible
            for (int i = 0; i < LIGNES; i++)
            {
                for (int j = 0; j < COLONNES; j++)
                {
                    if (PlacementPossible[i, j])
                        return true;
                }
            }
            //sinon
            return false;
        }

        /// <summary>
        /// Méthode qui vérifie si un jeton peut être placé a un position
        /// choisi. Elle met a jour les infos du coup (si il est valide), dans
        /// m_coups
        /// </summary>
        /// <param name="posY">Position Y à fournir.</param>
        /// <param name="posX">Position X à fournir.</param>
        /// <param name="couleurPlacee">Couleur de la personne/IA.</param>
        /// <returns></returns>
        public bool MouvementLegal(int posY, int posX, int couleurPlacee)
        {
            CouleurValide(couleurPlacee);
            CoordonneesValides(posY, posX);

            int ennemi = couleurPlacee * -1;

            //Vérification de si il y a un jeton ennemi

            //   7  0  1
            //   NW N NE  
            // 6 W  x  E 2
            //   SW S SE
            //   5  4  3
            if (this[posY, posX] == VIDE)
            {
                //Variables pour l'objet Coup qui sera crée
                bool[] orientation = new bool[8];
                int[] distance = new int[8];

                bool allyFound = false;

                // N
                if (posY - 2 >= 0 && this[posY - 1, posX] == ennemi) //Check ennemi affecté
                {
                    int nbEnnemiAffecte = 0;

                    do
                    {
                        //Sors de la boucle si la case est vide/non valide
                        if (posY - (nbEnnemiAffecte + 1) >= 0 && this[posY - (nbEnnemiAffecte + 1), posX] != VIDE)
                        {
                            if (this[(posY - (nbEnnemiAffecte + 1)), posX] == couleurPlacee)
                                allyFound = true;
                            else
                                nbEnnemiAffecte++;
                        }
                        else break;
                    } while (!allyFound);

                    if (allyFound) //Si le coup est legal et affecte le jeu
                    {
                        orientation[0] = allyFound;
                        distance[0] = nbEnnemiAffecte;
                        allyFound = false;
                    }
                }

                // NE
                if (posY - 2 >= 0 && posX + 2 < COLONNES && this[posY - 1, posX + 1] == ennemi) //Check ennemi affecté
                {
                    int nbEnnemiAffecte = 0;
                    int distCheck;

                    do
                    {
                        distCheck = nbEnnemiAffecte + 1;
                        //Sors de la boucle si la case est vide/non valide
                        if (posY - distCheck >= 0 && posX + distCheck < COLONNES && this[posY - distCheck, posX + distCheck] != VIDE)
                        {
                            if (this[posY - distCheck, posX + distCheck] == couleurPlacee)
                                allyFound = true;
                            else
                                nbEnnemiAffecte++;
                        }
                        else break;
                    } while (!allyFound);

                    if (allyFound) //Si le coup est legal et affecte le jeu
                    {
                        orientation[1] = allyFound;
                        distance[1] = nbEnnemiAffecte;
                        allyFound = false;
                    }
                }

                // E
                if (posX + 2 < COLONNES && this[posY, posX + 1] == ennemi) //Check ennemi affecté
                {
                    int nbEnnemiAffecte = 0;
                    int distCheck;

                    do
                    {
                        distCheck = nbEnnemiAffecte + 1;
                        //Sors de la boucle si la case est vide/non valide
                        if (posX + distCheck < COLONNES && this[posY, posX + distCheck] != VIDE)
                        {
                            if (this[posY, posX + distCheck] == couleurPlacee)
                                allyFound = true;
                            else
                                nbEnnemiAffecte++;
                        }
                        else break;
                    } while (!allyFound);

                    if (allyFound) //Si le coup est legal et affecte le jeu
                    {
                        orientation[2] = allyFound;
                        distance[2] = nbEnnemiAffecte;
                        allyFound = false;
                    }
                }

                // SE
                if (posY + 2 < LIGNES && posX + 2 < COLONNES && this[posY + 1, posX + 1] == ennemi) //Check ennemi affecté
                {
                    int nbEnnemiAffecte = 0;
                    int distCheck;
                    //I cause problemsjiik
                    do
                    {
                        distCheck = nbEnnemiAffecte + 1;

                        if (posY + distCheck == 8)
                        {

                        }

                        //Sors de la boucle si la case est vide/non valide
                        if (posY + distCheck < LIGNES && posX + distCheck < COLONNES && this[posY + distCheck, posX + distCheck] != VIDE)
                        {
                            if (this[posY + distCheck, posX + distCheck] == couleurPlacee)
                                allyFound = true;
                            else
                                nbEnnemiAffecte++;
                        }
                        else break;
                    } while (!allyFound);

                    if (allyFound) //Si le coup est legal et affecte le jeu
                    {
                        orientation[3] = allyFound;
                        distance[3] = nbEnnemiAffecte;
                        allyFound = false;
                    }
                }

                // S
                if (posY + 2 < LIGNES && this[posY + 1, posX] == ennemi) //Check ennemi affecté
                {
                    int nbEnnemiAffecte = 0;
                    int distCheck;

                    do
                    {
                        if (posY == 8) throw new ArgumentException("PANIC");

                        distCheck = nbEnnemiAffecte + 1;
                        //Sors de la boucle si la case est vide/non valide
                        if (posY + distCheck < LIGNES && this[posY + distCheck, posX] != VIDE)
                        {
                            if (this[posY + distCheck, posX] == couleurPlacee)
                                allyFound = true;
                            else
                                nbEnnemiAffecte++;
                        }
                        else break;
                    } while (!allyFound);

                    if (allyFound) //Si le coup est legal et affecte le jeu
                    {
                        orientation[4] = allyFound;
                        distance[4] = nbEnnemiAffecte;
                        allyFound = false;
                    }
                }

                // SW
                if (posY + 2 < LIGNES && posX - 2 >= 0 && this[posY + 1, posX - 1] == ennemi) //Check ennemi affecté
                {
                    int nbEnnemiAffecte = 0;
                    int distCheck;

                    do
                    {
                        distCheck = nbEnnemiAffecte + 1;
                        //Sors de la boucle si la case est vide/non valide
                        if (posY + distCheck < LIGNES && posX - distCheck >= 0 && this[posY + distCheck, posX - distCheck] != VIDE)
                        {
                            if (this[posY + distCheck, posX - distCheck] == couleurPlacee)
                                allyFound = true;
                            else
                                nbEnnemiAffecte++;
                        }
                        else break;
                    } while (!allyFound);

                    if (allyFound) //Si le coup est legal et affecte le jeu
                    {
                        orientation[5] = allyFound;
                        distance[5] = nbEnnemiAffecte;
                        allyFound = false;
                    }
                }

                // W
                if (posX - 2 >= 0 && this[posY, posX - 1] == ennemi) //Check ennemi affecté
                {
                    int nbEnnemiAffecte = 0;
                    int distCheck;

                    do
                    {
                        distCheck = nbEnnemiAffecte + 1;
                        //Sors de la boucle si la case est vide/non valide
                        if (posX - distCheck >= 0 && this[posY, posX - distCheck] != VIDE)
                        {
                            if (this[posY, posX - distCheck] == couleurPlacee)
                                allyFound = true;
                            else
                                nbEnnemiAffecte++;
                        }
                        else break;
                    } while (!allyFound);

                    if (allyFound) //Si le coup est legal et affecte le jeu
                    {
                        orientation[6] = allyFound;
                        distance[6] = nbEnnemiAffecte;
                        allyFound = false;
                    }
                }

                // NW
                if (posY - 2 >= 0 && posX - 2 >= 0 && this[posY - 1, posX - 1] == ennemi) //Check ennemi affecté
                {
                    int nbEnnemiAffecte = 0;
                    int distCheck;

                    do
                    {
                        distCheck = nbEnnemiAffecte + 1;
                        //Sors de la boucle si la case est vide/non valide
                        if (posY - distCheck >= 0 && posX - distCheck >= 0 && this[posY - distCheck, posX - distCheck] != VIDE)
                        {
                            if (this[posY - distCheck, posX - distCheck] == couleurPlacee)
                                allyFound = true;
                            else
                                nbEnnemiAffecte++;
                        }
                        else break;
                    } while (!allyFound);

                    if (allyFound) //Si le coup est legal et affecte le jeu
                    {
                        orientation[7] = allyFound;
                        distance[7] = nbEnnemiAffecte;
                        allyFound = false;
                    }
                }


                //Vérification si au moins une orientation est valide.
                for (int i = 0; i < 8; i++)
                {
                    if (orientation[i])
                    {
                        m_coups[posY, posX] = new Coup(orientation, distance);
                        return true;
                    }

                }
            }
            //Si il y a sortie de cette boucle, il n'y a pas d'orientation valide.
            return false;
        }

        /// <summary>
        /// Méthode qui place un jeton sur le tableau et retourne les
        /// jetons affectés
        /// </summary>
        /// <param name="posX">Position X du jeton placé</param>
        /// <param name="posY">Position Y du jeton placé</param>
        /// <param name="couleurPlace">Couleur du jeton qui es placé (Seul 0 et 1 est valide)</param>
        /// <returns></returns>
        public bool PlacerJeton(int posY, int posX, int couleurPlacee)
        {
            //Validation
            CouleurValide(couleurPlacee);
            CoordonneesValides(posY, posX);

            if (PlacementPossible[posY, posX])
            {
                this[posY, posX] = couleurPlacee;
                Coup coupJouer = m_coups[posY, posX];
                int jetonsRetourne = 0;

                if (coupJouer.Nord(ref jetonsRetourne))
                {
                    for (int i = 1; i <= jetonsRetourne; i++)
                    {
                        this[posY - i, posX] = couleurPlacee;
                    }
                }

                if (coupJouer.NordEst(ref jetonsRetourne))
                {
                    for (int i = 1; i <= jetonsRetourne; i++)
                    {
                        this[posY - i, posX + i] = couleurPlacee;
                    }
                }

                if (coupJouer.Est(ref jetonsRetourne))
                {
                    for (int i = 1; i <= jetonsRetourne; i++)
                    {
                        this[posY, posX + i] = couleurPlacee;
                    }
                }

                if (coupJouer.SudEst(ref jetonsRetourne))
                {
                    for (int i = 1; i <= jetonsRetourne; i++)
                    {
                        this[posY + i, posX + i] = couleurPlacee;
                    }
                }

                if (coupJouer.Sud(ref jetonsRetourne))
                {
                    for (int i = 1; i <= jetonsRetourne; i++)
                    {
                        this[posY + i, posX] = couleurPlacee;
                    }
                }

                if (coupJouer.SudOuest(ref jetonsRetourne))
                {
                    for (int i = 1; i <= jetonsRetourne; i++)
                    {
                        this[posY + i, posX - i] = couleurPlacee;
                    }
                }

                if (coupJouer.Ouest(ref jetonsRetourne))
                {
                    for (int i = 1; i <= jetonsRetourne; i++)
                    {
                        this[posY, posX - i] = couleurPlacee;
                    }
                }

                if (coupJouer.NordOuest(ref jetonsRetourne))
                {
                    for (int i = 1; i <= jetonsRetourne; i++)
                    {
                        this[posY - i, posX - i] = couleurPlacee;
                    }
                }

                return true;
            }
            return false;
        }

        private bool CoupGagnant(Coordonnee positionEnVerification, int couleur)
        {
            Tableau tableauEnSimulation = new Tableau(this);
            tableauEnSimulation.PlacerJeton(positionEnVerification.X, positionEnVerification.Y, couleur);
            if (couleur == BLANC)
            {
                return (tableauEnSimulation.TrouverCoupsPermis(NOIR).Count == 0);
            }

            return (tableauEnSimulation.TrouverCoupsPermis(BLANC).Count == 0);

        }

        public List<Coordonnee> TrouverCoupsPermis(int couleur)
        {
            List<Coordonnee> coupsPermis = new List<Coordonnee>();
            for (int i = 0; i < LIGNES; i++)
            {
                for (int j = 0; j < COLONNES; j++)
                {
                    Coordonnee position = new Coordonnee(i, j);
                    AjouterCoupSiPermit(position, coupsPermis, couleur);
                }
            }
            return coupsPermis;
        }

        private void AjouterCoupSiPermit(Coordonnee position, List<Coordonnee> coupsPermis, int couleur)
        {
            // Si la case est libre
            if (MouvementLegal(position.Y, position.X, couleur))
            {
                // On trouve les coups permis
                List<Coordonnee> coupsValides = ConverserionCoupIA();

                if (CoupEstPresentDansListe(position, coupsValides))
                {
                    coupsPermis.Add(position);
                }

            }
        }

        private bool CoupEstPresentDansListe(Coordonnee coup, List<Coordonnee> lstCoups)
        {
            foreach (Coordonnee c in lstCoups)
            {
                if (coup == c)
                {
                    return true;
                }
            }
            return false;
        }

        private List<Coordonnee> TrouverCasesValides(Coordonnee positionInitiale, int couleur)
        {
            List<Coordonnee> coupsPermis = new List<Coordonnee>();

            if (NouveauTour(couleur))
            {
                for (int i = 1; i <= LIGNES; i++)
                {
                    for (int j = 1; j <= COLONNES; j++)
                    {
                        if (MouvementLegal(i,j,couleur))
                        {
                            Coordonnee placementPossible = new Coordonnee(i, j);
                            coupsPermis.Add(placementPossible);
                        }
                    }
                }
            }

            return coupsPermis;
        }

        public Coordonnee TrouverCoupGagnant(List<Coordonnee> lstCoups, int joueur)
        {
            foreach (Coordonnee coup in lstCoups)
            {
                if (CoupGagnant(coup, joueur))
                {
                    return coup;
                }
            }
            return new Coordonnee(0, 0);
        }

        public bool ListeCoupsContientCoupGagnant(List<Coordonnee> lstCoups, int joueur)
        {
            foreach (Coordonnee coup in lstCoups)
            {
                if (CoupGagnant(coup, joueur))
                {
                    return true;
                }
            }
            return false;
        }
        public Coordonnee TrouverCoin(List<Coordonnee> lstPositions)
        {
            Coordonnee coin = new Coordonnee(lstPositions[0].X, lstPositions[0].Y);
            foreach (Coordonnee position in lstPositions)
            {
                if (EstCoin(position))
                {
                    coin.X = position.X;
                    coin.Y = position.Y;
                }
            }
            return coin;
        }
        public bool EstCoin(Coordonnee position)
        {
            return (EstCoinHautGauche(position) || EstCoinHautDroite(position) || EstCoinBasDroite(position) || EstCoinBasGauche(position));
        }

        private bool EstCoinHautGauche(Coordonnee position)
        {
            return (position.X == 1 && position.Y == 1);
        }
        private bool EstCoinHautDroite(Coordonnee position)
        {
            return (position.X == LIGNES && position.Y == 1);
        }
        private bool EstCoinBasDroite(Coordonnee position)
        {
            return (position.X == LIGNES && position.Y == LIGNES);
        }
        private bool EstCoinBasGauche(Coordonnee position)
        {
            return (position.X == 1 && position.Y == LIGNES);
        }

        public List<Coordonnee> ConverserionCoupIA()
        {
            List<Coordonnee> list = new List<Coordonnee>();

            for (int i = 0; i < COLONNES; i++)
            {
                for (int j = 0; j < LIGNES; j++)
                {
                    if (PlacementPossible[i, j])
                    {
                        Coordonnee coordonnee = new Coordonnee(j, i);
                        list.Add(coordonnee);
                    }
                }
            }

            return list;
        }

    }
}
