﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace othello
{
    public partial class frmRecords : Form
    {
        public frmRecords()
        {
            InitializeComponent();
        }

        private void BtnFermerPage_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void frmRecords_Load(object sender, EventArgs e)
        {
            FileInfo fichier = new FileInfo("C:\\images\\fiche_records.txt");
            if (File.Exists("C:\\images\\fiche_records.txt"))
            {
                //Déclaration d'une variable pour lire le fichier de fiches.
                StreamReader listeDesParties = new StreamReader("C:\\images\\fiche_records.txt");

                //Déclaration des variables.
                string strLigne;
                string[] strInfosJoueur;
                Joueur unJoueur;

                while ((strLigne = listeDesParties.ReadLine()) != null)
                {
                    strInfosJoueur = strLigne.Split(';');

                    unJoueur = new Joueur(strInfosJoueur[0], int.Parse(strInfosJoueur[1]));

                    unJoueur.NbJetons = byte.Parse(strInfosJoueur[2]);

                    this.lstRecords.Items.Add(unJoueur);
                }

                //Fermeture du fichier.
                listeDesParties.Close();
            }
            
        }

       
    }
}
