﻿namespace othello
{
    partial class frmRecords
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRecords));
            this.label1 = new System.Windows.Forms.Label();
            this.btnFermerPage = new System.Windows.Forms.Button();
            this.lstRecords = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(141, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Records à battre:";
            // 
            // btnFermerPage
            // 
            this.btnFermerPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFermerPage.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnFermerPage.Location = new System.Drawing.Point(88, 362);
            this.btnFermerPage.Margin = new System.Windows.Forms.Padding(2);
            this.btnFermerPage.Name = "btnFermerPage";
            this.btnFermerPage.Size = new System.Drawing.Size(258, 36);
            this.btnFermerPage.TabIndex = 2;
            this.btnFermerPage.Text = "Fermer la page";
            this.btnFermerPage.UseVisualStyleBackColor = true;
            this.btnFermerPage.Click += new System.EventHandler(this.BtnFermerPage_Click);
            // 
            // lstRecords
            // 
            this.lstRecords.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.lstRecords.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstRecords.FormattingEnabled = true;
            this.lstRecords.ItemHeight = 20;
            this.lstRecords.Location = new System.Drawing.Point(9, 52);
            this.lstRecords.Margin = new System.Windows.Forms.Padding(4);
            this.lstRecords.Name = "lstRecords";
            this.lstRecords.Size = new System.Drawing.Size(439, 304);
            this.lstRecords.TabIndex = 3;
            // 
            // frmRecords
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(459, 406);
            this.Controls.Add(this.lstRecords);
            this.Controls.Add(this.btnFermerPage);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmRecords";
            this.Text = "Records";
            this.Load += new System.EventHandler(this.frmRecords_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnFermerPage;
        private System.Windows.Forms.ListBox lstRecords;
    }
}