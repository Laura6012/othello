﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace othello
{
    public enum NiveauDifficulte { Facile, Normal, Difficile, Professionnel }
    
    public class IA_Othello
    {
        //Attributs
        private Joueur m_JoueurIA;
        private Tableau m_tableauJeu;
        private NiveauDifficulte m_difficulte;
        private int m_couleurIA;
        private int[,] m_scoreRegions;

        //Accesseurs
        private Tableau Jeu { 
            get { return m_tableauJeu; }
            set { m_tableauJeu = value; } 
        }
        public Joueur JoueurIA { get { return m_JoueurIA; } }
        private int CouleurIA { 
            get {return m_couleurIA; }
            set {m_couleurIA = value; } 
        }
        public NiveauDifficulte Difficulte { get { return m_difficulte; } }


        private int[,] ScoreRegions { 
            get {return m_scoreRegions; }
            set {m_scoreRegions = value; }
        }


        //Constructeur
        public IA_Othello(Tableau tableauJeu ,NiveauDifficulte Difficulte, int Couleur)
        {
            Jeu = tableauJeu;
            m_difficulte = Difficulte;
            CouleurIA = Couleur;

            m_JoueurIA = new Joueur("IA", Couleur);
            initialiserValeurCases();
        }

        /// <summary>  
        /// Méthode qui retourne la valeur des cases (en fonction de l'important)
        /// </summary>
        private void initialiserValeurCases()
        {
            ScoreRegions = new int[,] {
                { 100,-10,10, 6, 6, 10,-10,100 },
                { -10,-20, 1, 2, 2,  1,-20,-10 },
                {  11,  1, 5, 4, 4,  5,  1, 11 },
                {   6,  2, 4, 2, 2,  4,  2,  6 },
                {   6,  2, 4, 2, 2,  4,  2,  6 },
                {  11,  1, 5, 4, 4,  5,  1, 11 },
                { -10,-20, 1, 2, 2,  1,-20,-10 },
                { 100,-10,10, 6, 6, 10,-10,100 },
            };
        }

        /// <summary>
        /// Méthode qui trouve la meilleure position pour jouer 
        /// </summary>
        /// <param name="nbCoupsEnAvance"></param>
        /// <returns> Tableau int de 2 valeur qui indiquent la 
        /// coordonnée de la position, 0 pour ligne, 1 pour colonne</returns>
        public Coordonnee TrouverMeilleurPosition(Tableau tableauJeu, int nbCoupsEnAvance, List<Coordonnee> lstCasesPossibles)
        {
            if (Difficulte > NiveauDifficulte.Facile)
            {
                if (tableauJeu.ListeCoupsContientCoupGagnant(lstCasesPossibles, CouleurIA) == false)
                {
                    List<ValeurCoup> lstCoups = new List<ValeurCoup>();
                    // Attribut un score selon le nombre de coups en avance voulu pour chaque position , ce qui forme un coup pour chaque positions valides
                    RemplirListeCoups(lstCoups, lstCasesPossibles, nbCoupsEnAvance, tableauJeu);
                    // On attribut le coup avec le plus gros score à la variable meilleurCoup
                    Coordonnee meilleurPosition = PositionMaxCoups(lstCoups);
                    // On retourne la position du meilleur coup
                    return meilleurPosition;
                }
                else
                {
                    return tableauJeu.TrouverCoin(lstCasesPossibles);
                }
            }
            else
            {
                Random rndCoup = new Random(DateTime.Now.Millisecond);

                return lstCasesPossibles[rndCoup.Next(0, lstCasesPossibles.Count)];
            }
        }

        private void RemplirListeCoups(List<ValeurCoup> lstCoups, List<Coordonnee> lstCasesPossibles, int nbCoupsEnAvance, Tableau tableauJeu)
        {
            foreach (Coordonnee position in lstCasesPossibles)
            {
                ValeurCoup coup = new ValeurCoup(position, ScoreApresXCoupSimule(position, nbCoupsEnAvance, tableauJeu));
                lstCoups.Add(coup);
            }
        }

        private int ScoreApresXCoupSimule(Coordonnee positionIAPossibleEnVerification, int NbCoupASimuler,Tableau jeuDepart)
        {
            int scoreDePosition = 0;
            Tableau jeuEnSimulation = new Tableau(jeuDepart);
            //On joue la position en vérification sur le tableau copie
            jeuEnSimulation.PlacerJeton(positionIAPossibleEnVerification.X, positionIAPossibleEnVerification.Y , Tableau.BLANC);
            //On calcul la valeur x de cette possition en vérification
            //x est le nombre de coup plus tard (Nombre de fois à simuler) 
            for (int i = 0; i < NbCoupASimuler; i++)
            {
                List<ValeurCoup> lstCoup = new List<ValeurCoup>();

                // Un coup sur deux sera le tour du Humain donc on minimized le coup
                if (i % 2 == 0) // "Minimizer" (Humain)
                {
                    SimulerMinMax(jeuEnSimulation, Tableau.NOIR, ref lstCoup, ref scoreDePosition);
                }
                else // "Maximizer" (AI)
                {
                    SimulerMinMax(jeuEnSimulation, CouleurIA, ref lstCoup, ref scoreDePosition);
                }
            }

            return scoreDePosition;
        }

        private void SimulerMinMax(Tableau TableauEnSimulation, int couleurEnSimulation,ref List<ValeurCoup> lstCoup, ref int scoreDePosition)
        {
            //Évalue chaque coup possible
            foreach (Coordonnee position in TableauEnSimulation.TrouverCoupsPermis(Tableau.BLANC))
            {
                int score = 0;
                //Récupère le score d'IA après le coup actuel
                score = ScoreIAApresCoupSimule(TableauEnSimulation, position, couleurEnSimulation);
                //On garde la position qu'on évalue et ainsi que son score
                ValeurCoup coup = new ValeurCoup(position, score);
                //Ajouter à la liste qui sera évaluer plus tard
                lstCoup.Add(coup);
            }

            if (lstCoup.Count > 0)
            {
                Coordonnee positionChoisie = new Coordonnee(0, 0);

                //Tour de IA
                if (couleurEnSimulation == Tableau.BLANC)
                {
                    // On choisi donc le coup avec le score le plus élevé pour l'AI ,car c'est le meilleur choix pour lui et c'est à son tour de jouer ,donc il est logique de dire que l'AI doit
                    // prendre le meilleur des coup lorsque c'est à son tour
                    positionChoisie = PositionMaxCoups(lstCoup);
                    // On met à jour le score de la position avec le score le plus réçent que nous avons
                    // Si la simulation devait s'arrêter maintenant ce serait à l'AI de jouer donc celui-ci prendrait le coup qui l'avantagerait le plus
                    // Donc il choisirait le coup avec le score du AI le plus élevé
                    scoreDePosition = MaxScoreCoups(lstCoup);
                }
                else //Tour de humain, l'invers avec IA, on prend la position qui retourne le moins de score
                {
                    // On choisi donc le coup avec le score le plus faible pour l'AI ,car c'est le meilleur choix pour le joueur qui doit jouer actuellement (l'humain)
                    positionChoisie = PositionMinCoups(lstCoup);
                    // On met à jour le score de la position avec le score le plus réçent que nous avons
                    // Si la simulation devait s'arrêter maintenant ce serait à l'humain de jouer donc celui-ci prendrait le coup qui avantage le moins l'AI
                    // Donc il choisirait le coup avec le score du AI le plus faible
                    scoreDePosition = MinScoreCoups(lstCoup);
                }

                //Lorsque la position est choisi, on joue le coup
                TableauEnSimulation.PlacerJeton(positionChoisie.X, positionChoisie.Y, couleurEnSimulation);
            }
        }

        /// <summary>
        /// Méthode qui trouve le coup qui donne le plus 
        /// de point dans la liste de coup fourni
        /// </summary>
        /// <param name="lstCoups"></param>
        /// <returns>Le coup qui donne le plus de point</returns>
        private Coordonnee PositionMaxCoups(List<ValeurCoup> lstCoups)
        {
            if (lstCoups.Count > 0)
            {
                ValeurCoup maxCoup = lstCoups[0];
                for (int i = 1; i < lstCoups.Count; i++)
                {
                    if (lstCoups[i].Score > maxCoup.Score)
                    {
                        maxCoup = lstCoups[i];
                    }
                }
                return maxCoup.Position;
            }

            return null;
        }

        /// <summary>
        /// Méthode qui trouve le coup qui donne le moins 
        /// de point dans la liste de coup fourni
        /// </summary>
        /// <param name="lstCoups"></param>
        /// <returns>Le coup qui donne le moins de point</returns>
        private Coordonnee PositionMinCoups(List<ValeurCoup> lstCoups)
        {
            ValeurCoup minCoup = lstCoups[0];
            for (int i = 1; i < lstCoups.Count; i++)
            {
                if (lstCoups[i].Score < minCoup.Score)
                {
                    minCoup = lstCoups[i];
                }
            }
            return minCoup.Position;
        }

        /// <summary>
        /// Méthode qui retourn le plus élévé score selon la position
        /// </summary>
        /// <param name="lstCoups"></param>
        /// <returns>Le score le plus élévé</returns>
        private int MaxScoreCoups(List<ValeurCoup> lstCoups)
        {
            ValeurCoup maxCoup = lstCoups[0];
            for (int i = 1; i < lstCoups.Count; i++)
            {
                if (lstCoups[i].Score > maxCoup.Score)
                {
                    maxCoup = lstCoups[i];
                }
            }
            return maxCoup.Score;
        }

        /// <summary>
        /// Méthode qui retourn le moins élévé score selon la position
        /// </summary>
        /// <param name="lstCoups"></param>
        /// <returns>Le score le moins élévé</returns>
        private int MinScoreCoups(List<ValeurCoup> lstCoups)
        {
            ValeurCoup minCoup = lstCoups[0];
            for (int i = 1; i < lstCoups.Count; i++)
            {
                if (lstCoups[i].Score < minCoup.Score)
                {
                    minCoup = lstCoups[i];
                }
            }
            return minCoup.Score;
        }

        private int CalculerScoreIASelonRegions(Tableau tableau)
        {
            int scoreIA = 0;
            int scoreHumain = 0;

            for (int i = 1; i < Tableau.LIGNES; i++)
            {
                for (int j = 1; j < Tableau.COLONNES; j++)
                {
                    if (tableau[i, j] != 0)
                    {
                        if (tableau[i, j] == 1)
                        {
                            scoreIA += ScoreRegions[i - 1, j - 1];
                        }
                        else if (tableau[i, j] == -1)
                        {
                            scoreHumain += ScoreRegions[i - 1, j - 1];
                        }
                    }
                }
            }

            return scoreIA - scoreHumain;
        }

        private int CalculerScoreSelonNbPions(Tableau tableau)
        {
            int nbPionIA = tableau.NbJetonsBlancs;
            int nbPionHumain = tableau.NbJetonsNoirs;

            return nbPionIA - nbPionHumain;
        }

        private int ScoreIAApresCoupSimule(Tableau tableauEnSimulation, Coordonnee coupAFaire, int couleurEnSimulation)
        {
            int score = 0;
            Tableau NouveauTabSimule = new Tableau(tableauEnSimulation);
            //Inverser les pions affecter par le coup
            tableauEnSimulation.PlacerJeton(coupAFaire.X, coupAFaire.Y, Tableau.BLANC);
            score += CalculerScoreIASelonRegions(tableauEnSimulation);
            score += CalculerScoreSelonNbPions(tableauEnSimulation);
            return score;
        }
    } 
}


