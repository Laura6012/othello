﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace othello
{
    class ValeurCoup
    {
        public Coordonnee Position { get; set; }

        public int Score { get; set; }

        public ValeurCoup(Coordonnee position, int score)
        {
            Position = position;
            Score = score;
        }
    }
}
