﻿namespace othello
{
    partial class frmJeu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmJeu));
            this.lblNomJeu = new System.Windows.Forms.Label();
            this.txtNomJoueur = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtVosPions = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPionsAdversaire = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTourDe = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTempsRestant = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCasesVides = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.flpPbo = new System.Windows.Forms.FlowLayoutPanel();
            this.tmrTemps = new System.Windows.Forms.Timer(this.components);
            this.lstCoupJoue = new System.Windows.Forms.ListBox();
            this.pboImageJoueur = new System.Windows.Forms.PictureBox();
            this.pboImageAdv = new System.Windows.Forms.PictureBox();
            this.txtNomAdversaire = new System.Windows.Forms.TextBox();
            this.pboTourDe = new System.Windows.Forms.PictureBox();
            this.lblNomAdversaire = new System.Windows.Forms.Label();
            this.btnRecommencer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pboImageJoueur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboImageAdv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboTourDe)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNomJeu
            // 
            this.lblNomJeu.AutoSize = true;
            this.lblNomJeu.BackColor = System.Drawing.Color.Transparent;
            this.lblNomJeu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomJeu.ForeColor = System.Drawing.Color.White;
            this.lblNomJeu.Location = new System.Drawing.Point(756, 7);
            this.lblNomJeu.Name = "lblNomJeu";
            this.lblNomJeu.Size = new System.Drawing.Size(97, 20);
            this.lblNomJeu.TabIndex = 64;
            this.lblNomJeu.Text = "Votre nom:";
            // 
            // txtNomJoueur
            // 
            this.txtNomJoueur.BackColor = System.Drawing.Color.Ivory;
            this.txtNomJoueur.Enabled = false;
            this.txtNomJoueur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomJoueur.ForeColor = System.Drawing.Color.Black;
            this.txtNomJoueur.Location = new System.Drawing.Point(760, 31);
            this.txtNomJoueur.Name = "txtNomJoueur";
            this.txtNomJoueur.ReadOnly = true;
            this.txtNomJoueur.Size = new System.Drawing.Size(159, 26);
            this.txtNomJoueur.TabIndex = 65;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(760, 729);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(169, 32);
            this.button1.TabIndex = 67;
            this.button1.Text = "Records";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // btnQuitter
            // 
            this.btnQuitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuitter.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnQuitter.Location = new System.Drawing.Point(935, 729);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(167, 32);
            this.btnQuitter.TabIndex = 68;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.BtnQuitter_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(756, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 20);
            this.label1.TabIndex = 69;
            this.label1.Text = "Nb jetons:";
            // 
            // txtVosPions
            // 
            this.txtVosPions.BackColor = System.Drawing.Color.Ivory;
            this.txtVosPions.Enabled = false;
            this.txtVosPions.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVosPions.ForeColor = System.Drawing.Color.Black;
            this.txtVosPions.Location = new System.Drawing.Point(760, 107);
            this.txtVosPions.Name = "txtVosPions";
            this.txtVosPions.ReadOnly = true;
            this.txtVosPions.Size = new System.Drawing.Size(60, 26);
            this.txtVosPions.TabIndex = 70;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(943, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 20);
            this.label2.TabIndex = 71;
            this.label2.Text = "Nb jetons:";
            // 
            // txtPionsAdversaire
            // 
            this.txtPionsAdversaire.BackColor = System.Drawing.Color.Ivory;
            this.txtPionsAdversaire.Enabled = false;
            this.txtPionsAdversaire.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPionsAdversaire.ForeColor = System.Drawing.Color.Black;
            this.txtPionsAdversaire.Location = new System.Drawing.Point(947, 107);
            this.txtPionsAdversaire.Name = "txtPionsAdversaire";
            this.txtPionsAdversaire.ReadOnly = true;
            this.txtPionsAdversaire.Size = new System.Drawing.Size(58, 26);
            this.txtPionsAdversaire.TabIndex = 72;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(783, 193);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 20);
            this.label3.TabIndex = 73;
            this.label3.Text = "Au tour de:";
            // 
            // txtTourDe
            // 
            this.txtTourDe.BackColor = System.Drawing.Color.Ivory;
            this.txtTourDe.Enabled = false;
            this.txtTourDe.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTourDe.ForeColor = System.Drawing.Color.Black;
            this.txtTourDe.Location = new System.Drawing.Point(887, 190);
            this.txtTourDe.Name = "txtTourDe";
            this.txtTourDe.ReadOnly = true;
            this.txtTourDe.Size = new System.Drawing.Size(108, 26);
            this.txtTourDe.TabIndex = 74;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(756, 334);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 20);
            this.label4.TabIndex = 75;
            this.label4.Text = "Temps restant:";
            // 
            // txtTempsRestant
            // 
            this.txtTempsRestant.BackColor = System.Drawing.Color.Ivory;
            this.txtTempsRestant.Enabled = false;
            this.txtTempsRestant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTempsRestant.ForeColor = System.Drawing.Color.Black;
            this.txtTempsRestant.Location = new System.Drawing.Point(891, 331);
            this.txtTempsRestant.Name = "txtTempsRestant";
            this.txtTempsRestant.ReadOnly = true;
            this.txtTempsRestant.Size = new System.Drawing.Size(213, 26);
            this.txtTempsRestant.TabIndex = 76;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(756, 281);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(199, 20);
            this.label5.TabIndex = 77;
            this.label5.Text = "Nombre de cases vides:";
            // 
            // txtCasesVides
            // 
            this.txtCasesVides.BackColor = System.Drawing.Color.Ivory;
            this.txtCasesVides.Enabled = false;
            this.txtCasesVides.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCasesVides.ForeColor = System.Drawing.Color.Black;
            this.txtCasesVides.Location = new System.Drawing.Point(961, 278);
            this.txtCasesVides.Name = "txtCasesVides";
            this.txtCasesVides.ReadOnly = true;
            this.txtCasesVides.Size = new System.Drawing.Size(141, 26);
            this.txtCasesVides.TabIndex = 78;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(26, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 20);
            this.label6.TabIndex = 79;
            this.label6.Text = "A";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(27, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 20);
            this.label7.TabIndex = 80;
            this.label7.Text = "B";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(27, 249);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 20);
            this.label8.TabIndex = 81;
            this.label8.Text = "C";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(26, 346);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 20);
            this.label9.TabIndex = 82;
            this.label9.Text = "D";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(26, 431);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 20);
            this.label10.TabIndex = 83;
            this.label10.Text = "E";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(27, 517);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 20);
            this.label11.TabIndex = 84;
            this.label11.Text = "F";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(28, 601);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 20);
            this.label12.TabIndex = 85;
            this.label12.Text = "G";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(27, 673);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(22, 20);
            this.label13.TabIndex = 86;
            this.label13.Text = "H";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(88, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 20);
            this.label14.TabIndex = 87;
            this.label14.Text = "1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(171, 25);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 20);
            this.label15.TabIndex = 88;
            this.label15.Text = "2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(255, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(19, 20);
            this.label16.TabIndex = 89;
            this.label16.Text = "3";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(340, 25);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 20);
            this.label17.TabIndex = 90;
            this.label17.Text = "4";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(429, 25);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 20);
            this.label18.TabIndex = 91;
            this.label18.Text = "5";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(515, 25);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 20);
            this.label19.TabIndex = 92;
            this.label19.Text = "6";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(599, 25);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(19, 20);
            this.label20.TabIndex = 93;
            this.label20.Text = "7";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(685, 25);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(19, 20);
            this.label21.TabIndex = 94;
            this.label21.Text = "8";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(756, 381);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(113, 20);
            this.label22.TabIndex = 96;
            this.label22.Text = "Coups joués:";
            // 
            // flpPbo
            // 
            this.flpPbo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("flpPbo.BackgroundImage")));
            this.flpPbo.ForeColor = System.Drawing.Color.Black;
            this.flpPbo.Location = new System.Drawing.Point(53, 48);
            this.flpPbo.Margin = new System.Windows.Forms.Padding(2);
            this.flpPbo.Name = "flpPbo";
            this.flpPbo.Size = new System.Drawing.Size(682, 682);
            this.flpPbo.TabIndex = 95;
            // 
            // tmrTemps
            // 
            this.tmrTemps.Enabled = true;
            this.tmrTemps.Interval = 1000;
            this.tmrTemps.Tick += new System.EventHandler(this.tmrTemps_Tick);
            // 
            // lstCoupJoue
            // 
            this.lstCoupJoue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCoupJoue.FormattingEnabled = true;
            this.lstCoupJoue.ItemHeight = 20;
            this.lstCoupJoue.Location = new System.Drawing.Point(760, 404);
            this.lstCoupJoue.Name = "lstCoupJoue";
            this.lstCoupJoue.Size = new System.Drawing.Size(342, 284);
            this.lstCoupJoue.TabIndex = 97;
            // 
            // pboImageJoueur
            // 
            this.pboImageJoueur.Location = new System.Drawing.Point(847, 77);
            this.pboImageJoueur.Margin = new System.Windows.Forms.Padding(2);
            this.pboImageJoueur.Name = "pboImageJoueur";
            this.pboImageJoueur.Size = new System.Drawing.Size(71, 76);
            this.pboImageJoueur.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboImageJoueur.TabIndex = 98;
            this.pboImageJoueur.TabStop = false;
            // 
            // pboImageAdv
            // 
            this.pboImageAdv.Location = new System.Drawing.Point(1035, 77);
            this.pboImageAdv.Margin = new System.Windows.Forms.Padding(2);
            this.pboImageAdv.Name = "pboImageAdv";
            this.pboImageAdv.Size = new System.Drawing.Size(71, 76);
            this.pboImageAdv.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboImageAdv.TabIndex = 99;
            this.pboImageAdv.TabStop = false;
            // 
            // txtNomAdversaire
            // 
            this.txtNomAdversaire.BackColor = System.Drawing.Color.Ivory;
            this.txtNomAdversaire.Enabled = false;
            this.txtNomAdversaire.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomAdversaire.ForeColor = System.Drawing.Color.Black;
            this.txtNomAdversaire.Location = new System.Drawing.Point(947, 31);
            this.txtNomAdversaire.Name = "txtNomAdversaire";
            this.txtNomAdversaire.ReadOnly = true;
            this.txtNomAdversaire.Size = new System.Drawing.Size(159, 26);
            this.txtNomAdversaire.TabIndex = 100;
            // 
            // pboTourDe
            // 
            this.pboTourDe.Location = new System.Drawing.Point(1005, 165);
            this.pboTourDe.Margin = new System.Windows.Forms.Padding(2);
            this.pboTourDe.Name = "pboTourDe";
            this.pboTourDe.Size = new System.Drawing.Size(71, 76);
            this.pboTourDe.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboTourDe.TabIndex = 101;
            this.pboTourDe.TabStop = false;
            // 
            // lblNomAdversaire
            // 
            this.lblNomAdversaire.AutoSize = true;
            this.lblNomAdversaire.BackColor = System.Drawing.Color.Transparent;
            this.lblNomAdversaire.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomAdversaire.ForeColor = System.Drawing.Color.White;
            this.lblNomAdversaire.Location = new System.Drawing.Point(943, 7);
            this.lblNomAdversaire.Name = "lblNomAdversaire";
            this.lblNomAdversaire.Size = new System.Drawing.Size(138, 20);
            this.lblNomAdversaire.TabIndex = 102;
            this.lblNomAdversaire.Text = "Nom adversaire:";
            // 
            // btnRecommencer
            // 
            this.btnRecommencer.BackColor = System.Drawing.Color.White;
            this.btnRecommencer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecommencer.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnRecommencer.Location = new System.Drawing.Point(760, 691);
            this.btnRecommencer.Name = "btnRecommencer";
            this.btnRecommencer.Size = new System.Drawing.Size(342, 32);
            this.btnRecommencer.TabIndex = 103;
            this.btnRecommencer.Text = "Recommencer";
            this.btnRecommencer.UseVisualStyleBackColor = false;
            this.btnRecommencer.Click += new System.EventHandler(this.btnRecommencer_Click);
            // 
            // frmJeu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1121, 764);
            this.Controls.Add(this.btnRecommencer);
            this.Controls.Add(this.lblNomAdversaire);
            this.Controls.Add(this.pboTourDe);
            this.Controls.Add(this.txtNomAdversaire);
            this.Controls.Add(this.pboImageAdv);
            this.Controls.Add(this.pboImageJoueur);
            this.Controls.Add(this.lstCoupJoue);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.flpPbo);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtCasesVides);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtTempsRestant);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtTourDe);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPionsAdversaire);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtVosPions);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtNomJoueur);
            this.Controls.Add(this.lblNomJeu);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "frmJeu";
            this.Text = "Plateforme de jeu";
            this.TransparencyKey = System.Drawing.Color.Black;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmJeu_FormClosed);
            this.Load += new System.EventHandler(this.frmJeu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pboImageJoueur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboImageAdv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboTourDe)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblNomJeu;
        private System.Windows.Forms.TextBox txtNomJoueur;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtVosPions;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPionsAdversaire;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTourDe;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTempsRestant;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCasesVides;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;

        private System.Windows.Forms.FlowLayoutPanel flpPbo;
        private System.Windows.Forms.Timer tmrTemps;
        private System.Windows.Forms.ListBox lstCoupJoue;
        private System.Windows.Forms.PictureBox pboImageJoueur;
        private System.Windows.Forms.PictureBox pboImageAdv;
        private System.Windows.Forms.TextBox txtNomAdversaire;
        private System.Windows.Forms.PictureBox pboTourDe;
        private System.Windows.Forms.Label lblNomAdversaire;
        private System.Windows.Forms.Button btnRecommencer;
    }
}