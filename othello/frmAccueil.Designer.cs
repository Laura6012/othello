﻿namespace othello
{
    partial class frmAccueil
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAccueil));
            this.lblNomJoueur = new System.Windows.Forms.Label();
            this.txtNomJoueur = new System.Windows.Forms.TextBox();
            this.lblVotreCouleur = new System.Windows.Forms.Label();
            this.cboStylePions = new System.Windows.Forms.ComboBox();
            this.btnSelectionnerImgJoueur = new System.Windows.Forms.Button();
            this.btnCommencerPartie = new System.Windows.Forms.Button();
            this.pboImageJoueur = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.cboMode = new System.Windows.Forms.ComboBox();
            this.lblMode = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboDifficulte = new System.Windows.Forms.ComboBox();
            this.lblCouleurAdverse = new System.Windows.Forms.Label();
            this.pboImageAdversaire = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSelectionnerImgAdv = new System.Windows.Forms.Button();
            this.txtNomAdversaire = new System.Windows.Forms.TextBox();
            this.txtCouleurAdverse = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.radAdvCommence = new System.Windows.Forms.RadioButton();
            this.radJoueurCommence = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pboImageJoueur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboImageAdversaire)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNomJoueur
            // 
            this.lblNomJoueur.AutoSize = true;
            this.lblNomJoueur.BackColor = System.Drawing.Color.Transparent;
            this.lblNomJoueur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomJoueur.ForeColor = System.Drawing.Color.White;
            this.lblNomJoueur.Location = new System.Drawing.Point(16, 11);
            this.lblNomJoueur.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNomJoueur.Name = "lblNomJoueur";
            this.lblNomJoueur.Size = new System.Drawing.Size(63, 25);
            this.lblNomJoueur.TabIndex = 0;
            this.lblNomJoueur.Text = "Nom:";
            // 
            // txtNomJoueur
            // 
            this.txtNomJoueur.BackColor = System.Drawing.Color.Ivory;
            this.txtNomJoueur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomJoueur.ForeColor = System.Drawing.Color.Black;
            this.txtNomJoueur.Location = new System.Drawing.Point(23, 53);
            this.txtNomJoueur.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNomJoueur.Name = "txtNomJoueur";
            this.txtNomJoueur.Size = new System.Drawing.Size(407, 30);
            this.txtNomJoueur.TabIndex = 1;
            // 
            // lblVotreCouleur
            // 
            this.lblVotreCouleur.AutoSize = true;
            this.lblVotreCouleur.BackColor = System.Drawing.Color.Transparent;
            this.lblVotreCouleur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVotreCouleur.ForeColor = System.Drawing.Color.White;
            this.lblVotreCouleur.Location = new System.Drawing.Point(17, 217);
            this.lblVotreCouleur.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblVotreCouleur.Name = "lblVotreCouleur";
            this.lblVotreCouleur.Size = new System.Drawing.Size(141, 25);
            this.lblVotreCouleur.TabIndex = 2;
            this.lblVotreCouleur.Text = "Votre couleur";
            // 
            // cboStylePions
            // 
            this.cboStylePions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStylePions.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStylePions.FormattingEnabled = true;
            this.cboStylePions.Items.AddRange(new object[] {
            "Blancs",
            "Noirs"});
            this.cboStylePions.Location = new System.Drawing.Point(23, 245);
            this.cboStylePions.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboStylePions.Name = "cboStylePions";
            this.cboStylePions.Size = new System.Drawing.Size(405, 33);
            this.cboStylePions.TabIndex = 3;
            this.cboStylePions.SelectedIndexChanged += new System.EventHandler(this.cboStylePions_SelectedIndexChanged);
            // 
            // btnSelectionnerImgJoueur
            // 
            this.btnSelectionnerImgJoueur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectionnerImgJoueur.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSelectionnerImgJoueur.Location = new System.Drawing.Point(23, 602);
            this.btnSelectionnerImgJoueur.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSelectionnerImgJoueur.Name = "btnSelectionnerImgJoueur";
            this.btnSelectionnerImgJoueur.Size = new System.Drawing.Size(408, 44);
            this.btnSelectionnerImgJoueur.TabIndex = 7;
            this.btnSelectionnerImgJoueur.Text = "Sélectionner votre image";
            this.btnSelectionnerImgJoueur.UseVisualStyleBackColor = true;
            this.btnSelectionnerImgJoueur.Click += new System.EventHandler(this.btnSelectionnerImgJoueur_Click);
            // 
            // btnCommencerPartie
            // 
            this.btnCommencerPartie.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCommencerPartie.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCommencerPartie.Location = new System.Drawing.Point(252, 654);
            this.btnCommencerPartie.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCommencerPartie.Name = "btnCommencerPartie";
            this.btnCommencerPartie.Size = new System.Drawing.Size(407, 44);
            this.btnCommencerPartie.TabIndex = 9;
            this.btnCommencerPartie.Text = "Commencer la partie";
            this.btnCommencerPartie.UseVisualStyleBackColor = true;
            this.btnCommencerPartie.Click += new System.EventHandler(this.BtnCommencerPartie_Click);
            // 
            // pboImageJoueur
            // 
            this.pboImageJoueur.BackColor = System.Drawing.Color.Transparent;
            this.pboImageJoueur.Location = new System.Drawing.Point(91, 327);
            this.pboImageJoueur.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pboImageJoueur.Name = "pboImageJoueur";
            this.pboImageJoueur.Size = new System.Drawing.Size(269, 249);
            this.pboImageJoueur.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboImageJoueur.TabIndex = 6;
            this.pboImageJoueur.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(121, 298);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "Apercu de l\'image:";
            // 
            // btnQuitter
            // 
            this.btnQuitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuitter.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnQuitter.Location = new System.Drawing.Point(253, 708);
            this.btnQuitter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(407, 44);
            this.btnQuitter.TabIndex = 10;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.Button1_Click_1);
            // 
            // cboMode
            // 
            this.cboMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMode.FormattingEnabled = true;
            this.cboMode.ItemHeight = 25;
            this.cboMode.Items.AddRange(new object[] {
            "Joueur contre Joueur",
            "Joueur contre IA "});
            this.cboMode.Location = new System.Drawing.Point(21, 156);
            this.cboMode.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboMode.Name = "cboMode";
            this.cboMode.Size = new System.Drawing.Size(405, 33);
            this.cboMode.TabIndex = 2;
            this.cboMode.SelectedIndexChanged += new System.EventHandler(this.CboMode_SelectedIndexChanged);
            // 
            // lblMode
            // 
            this.lblMode.AutoSize = true;
            this.lblMode.BackColor = System.Drawing.Color.Transparent;
            this.lblMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMode.ForeColor = System.Drawing.Color.White;
            this.lblMode.Location = new System.Drawing.Point(17, 114);
            this.lblMode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(144, 25);
            this.lblMode.TabIndex = 2;
            this.lblMode.Text = "Mode de jeu: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(475, 114);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(172, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Niveau difficulté:";
            // 
            // cboDifficulte
            // 
            this.cboDifficulte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDifficulte.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDifficulte.FormattingEnabled = true;
            this.cboDifficulte.Items.AddRange(new object[] {
            "Facile",
            "Normal",
            "Difficile",
            "Professionnel"});
            this.cboDifficulte.Location = new System.Drawing.Point(480, 156);
            this.cboDifficulte.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboDifficulte.Name = "cboDifficulte";
            this.cboDifficulte.Size = new System.Drawing.Size(405, 33);
            this.cboDifficulte.TabIndex = 5;
            this.cboDifficulte.SelectedIndexChanged += new System.EventHandler(this.cboDifficulte_SelectedIndexChanged);
            // 
            // lblCouleurAdverse
            // 
            this.lblCouleurAdverse.AutoSize = true;
            this.lblCouleurAdverse.BackColor = System.Drawing.Color.Transparent;
            this.lblCouleurAdverse.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCouleurAdverse.ForeColor = System.Drawing.Color.White;
            this.lblCouleurAdverse.Location = new System.Drawing.Point(475, 217);
            this.lblCouleurAdverse.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCouleurAdverse.Name = "lblCouleurAdverse";
            this.lblCouleurAdverse.Size = new System.Drawing.Size(171, 25);
            this.lblCouleurAdverse.TabIndex = 9;
            this.lblCouleurAdverse.Text = "Couleur adverse";
            // 
            // pboImageAdversaire
            // 
            this.pboImageAdversaire.BackColor = System.Drawing.Color.Transparent;
            this.pboImageAdversaire.Location = new System.Drawing.Point(543, 326);
            this.pboImageAdversaire.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pboImageAdversaire.Name = "pboImageAdversaire";
            this.pboImageAdversaire.Size = new System.Drawing.Size(269, 249);
            this.pboImageAdversaire.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboImageAdversaire.TabIndex = 10;
            this.pboImageAdversaire.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(573, 298);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(192, 25);
            this.label5.TabIndex = 11;
            this.label5.Text = "Apercu de l\'image:";
            // 
            // btnSelectionnerImgAdv
            // 
            this.btnSelectionnerImgAdv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectionnerImgAdv.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSelectionnerImgAdv.Location = new System.Drawing.Point(480, 602);
            this.btnSelectionnerImgAdv.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSelectionnerImgAdv.Name = "btnSelectionnerImgAdv";
            this.btnSelectionnerImgAdv.Size = new System.Drawing.Size(408, 44);
            this.btnSelectionnerImgAdv.TabIndex = 8;
            this.btnSelectionnerImgAdv.Text = "Sélectionner image adversaire";
            this.btnSelectionnerImgAdv.UseVisualStyleBackColor = true;
            this.btnSelectionnerImgAdv.Click += new System.EventHandler(this.btnSelectionnerImgAdv_Click);
            // 
            // txtNomAdversaire
            // 
            this.txtNomAdversaire.BackColor = System.Drawing.Color.Ivory;
            this.txtNomAdversaire.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomAdversaire.ForeColor = System.Drawing.Color.Black;
            this.txtNomAdversaire.Location = new System.Drawing.Point(480, 53);
            this.txtNomAdversaire.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNomAdversaire.Name = "txtNomAdversaire";
            this.txtNomAdversaire.Size = new System.Drawing.Size(407, 30);
            this.txtNomAdversaire.TabIndex = 4;
            // 
            // txtCouleurAdverse
            // 
            this.txtCouleurAdverse.BackColor = System.Drawing.Color.Ivory;
            this.txtCouleurAdverse.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCouleurAdverse.ForeColor = System.Drawing.Color.Black;
            this.txtCouleurAdverse.Location = new System.Drawing.Point(480, 245);
            this.txtCouleurAdverse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCouleurAdverse.Name = "txtCouleurAdverse";
            this.txtCouleurAdverse.ReadOnly = true;
            this.txtCouleurAdverse.Size = new System.Drawing.Size(407, 30);
            this.txtCouleurAdverse.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(475, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 25);
            this.label1.TabIndex = 15;
            this.label1.Text = "Nom adversaire:";
            // 
            // radAdvCommence
            // 
            this.radAdvCommence.AutoSize = true;
            this.radAdvCommence.Location = new System.Drawing.Point(368, 400);
            this.radAdvCommence.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radAdvCommence.Name = "radAdvCommence";
            this.radAdvCommence.Size = new System.Drawing.Size(169, 21);
            this.radAdvCommence.TabIndex = 16;
            this.radAdvCommence.TabStop = true;
            this.radAdvCommence.Text = "Adversaire commence";
            this.radAdvCommence.UseVisualStyleBackColor = true;
            // 
            // radJoueurCommence
            // 
            this.radJoueurCommence.AutoSize = true;
            this.radJoueurCommence.Location = new System.Drawing.Point(368, 371);
            this.radJoueurCommence.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radJoueurCommence.Name = "radJoueurCommence";
            this.radJoueurCommence.Size = new System.Drawing.Size(145, 21);
            this.radJoueurCommence.TabIndex = 17;
            this.radJoueurCommence.TabStop = true;
            this.radJoueurCommence.Text = "Joueur commence";
            this.radJoueurCommence.UseVisualStyleBackColor = true;
            // 
            // frmAccueil
            // 
            this.AcceptButton = this.btnCommencerPartie;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(912, 767);
            this.Controls.Add(this.radJoueurCommence);
            this.Controls.Add(this.radAdvCommence);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCouleurAdverse);
            this.Controls.Add(this.txtNomAdversaire);
            this.Controls.Add(this.btnSelectionnerImgAdv);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pboImageAdversaire);
            this.Controls.Add(this.lblCouleurAdverse);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pboImageJoueur);
            this.Controls.Add(this.btnCommencerPartie);
            this.Controls.Add(this.btnSelectionnerImgJoueur);
            this.Controls.Add(this.cboDifficulte);
            this.Controls.Add(this.cboMode);
            this.Controls.Add(this.cboStylePions);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblMode);
            this.Controls.Add(this.lblVotreCouleur);
            this.Controls.Add(this.txtNomJoueur);
            this.Controls.Add(this.lblNomJoueur);
            this.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmAccueil";
            this.Text = "Inscrition";
            this.Load += new System.EventHandler(this.FrmAccueil_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pboImageJoueur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboImageAdversaire)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNomJoueur;
        private System.Windows.Forms.TextBox txtNomJoueur;
        private System.Windows.Forms.Label lblVotreCouleur;
        private System.Windows.Forms.ComboBox cboStylePions;
        private System.Windows.Forms.Button btnSelectionnerImgJoueur;
        private System.Windows.Forms.Button btnCommencerPartie;
        private System.Windows.Forms.PictureBox pboImageJoueur;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.ComboBox cboMode;
        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboDifficulte;
        private System.Windows.Forms.Label lblCouleurAdverse;
        private System.Windows.Forms.PictureBox pboImageAdversaire;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSelectionnerImgAdv;
        private System.Windows.Forms.TextBox txtNomAdversaire;
        private System.Windows.Forms.TextBox txtCouleurAdverse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radAdvCommence;
        private System.Windows.Forms.RadioButton radJoueurCommence;
    }
}

