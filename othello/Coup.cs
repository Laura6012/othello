﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace othello
{
    class Coup
    {
        private int[] m_distance;
        private bool[] m_orientation;

        /// <summary>
        /// Liste de booléennes qui représente l'orientation de
        /// la distance associée.
        /// 
        /// Les positions sont en fonction d'une bousolle, dans le sens
        /// d'une aiguille d'une montre.
        /// Nord : 0
        /// Nord-Est : 1
        /// Est : 2
        /// ...
        /// Nord-Ouest : 7
        /// </summary>
        public bool[] Orientation
        {
            get { return m_orientation; }
            set { m_orientation = value; }
        }

        /// <summary>
        /// Retourne le nombre de pions qui sont "flipper" à partir de 
        /// la position du pion placé. La distance est associée à la
        /// son orientation de même position
        /// </summary>
        public int[] Distance
        {
            get { return m_distance; }
            set { m_distance = value; }
        }

        /// <summary>
        /// Constructeur d'un coup
        /// </summary>
        /// <param name="orientation">Tableau[8] de bool qui représentent l'orientation</param>
        /// <param name="distance">Tableau[8] de int qui représentent la distance associé a l'orientation</param>
        public Coup(bool[] orientation, int[] distance)
        {
            this.m_orientation = new bool[8];
            this.m_distance = new int[8];

            for (int i = 0; i < 8; i++)
            {
                m_distance[i] = distance[i];
                m_orientation[i] = orientation[i];
            }
        }

        /// <summary>
        /// Constructeur par défaut. Tout est mis a faux ou 0.
        /// </summary>
        public Coup()
        {
            this.m_orientation = new bool[8];
            this.m_distance = new int[8];
        }

        /// <summary>
        /// Retourne si il y a un changement a cette orientation et la distance si vrai.
        /// </summary>
        /// <param name="distance">Variable dans laquel la distance est stocké.</param>
        /// <returns></returns>
        public bool Nord(ref int distance)
        {
            if (Orientation[0])
            {
                distance = Distance[0];
            }

            return Orientation[0];
        }
        /// <summary>
        /// Retourne si il y a un changement a cette orientation et la distance si vrai.
        /// </summary>
        /// <param name="distance">Variable dans laquel la distance est stocké.</param>
        /// <returns></returns>
        public bool NordEst(ref int distance)
        {
            if (Orientation[1])
            {
                distance = Distance[1];
            }

            return Orientation[1];
        }
        /// <summary>
        /// Retourne si il y a un changement a cette orientation et la distance si vrai.
        /// </summary>
        /// <param name="distance">Variable dans laquel la distance est stocké.</param>
        /// <returns></returns>
        public bool Est(ref int distance)
        {
            if (Orientation[2])
            {
                distance = Distance[2];
            }

            return Orientation[2];
        }
        /// <summary>
        /// Retourne si il y a un changement a cette orientation et la distance si vrai.
        /// </summary>
        /// <param name="distance">Variable dans laquel la distance est stocké.</param>
        /// <returns></returns>
        public bool SudEst(ref int distance)
        {
            if (Orientation[3])
            {
                distance = Distance[3];
            }

            return Orientation[3];
        }
        /// <summary>
        /// Retourne si il y a un changement a cette orientation et la distance si vrai.
        /// </summary>
        /// <param name="distance">Variable dans laquel la distance est stocké.</param>
        /// <returns></returns>
        public bool Sud(ref int distance)
        {
            if (Orientation[4])
            {
                distance = Distance[4];
            }

            return Orientation[4];
        }
        /// <summary>
        /// Retourne si il y a un changement a cette orientation et la distance si vrai.
        /// </summary>
        /// <param name="distance">Variable dans laquel la distance est stocké.</param>
        /// <returns></returns>
        public bool SudOuest(ref int distance)
        {
            if (Orientation[5])
            {
                distance = Distance[5];
            }

            return Orientation[5];
        }
        /// <summary>
        /// Retourne si il y a un changement a cette orientation et la distance si vrai.
        /// </summary>
        /// <param name="distance">Variable dans laquel la distance est stocké.</param>
        /// <returns></returns>
        public bool Ouest(ref int distance)
        {
            if (Orientation[6])
            {
                distance = Distance[6];
            }

            return Orientation[6];
        }
        /// <summary>
        /// Retourne si il y a un changement a cette orientation et la distance si vrai.
        /// </summary>
        /// <param name="distance">Variable dans laquel la distance est stocké.</param>
        /// <returns></returns>
        public bool NordOuest(ref int distance)
        {
            if (Orientation[7])
            {
                distance = Distance[7];
            }

            return Orientation[7];
        }
    }
}
