﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace othello
{
    public partial class frmJeu : Form
    {
        #region ATTRIBUTS
        //Booléens qui servent à vérifier si les deux joueurs n'ont plus de
        //coups disponibles
        private bool blnCoupsJouablesJoueur;
        private bool blnCoupsJouablesAdv;
        private bool blnCoupJouable;

        //Indique si le joueur commence au premier tour
        private bool blnJoueurCommence;

        private bool blnJcJ;
        private IA_Othello m_AI;

        //Reçoit le Joueur du Form Accueil
        private Joueur m_leJoueur;

        //Contient les pbo du Form
        private PictureBox[,] vectLesPbo;

        // PictureBox sélectionné.        
        private PictureBox m_pboSel;

        //Représentent le chemin des images sélectionnées par les joueurs
        private string strCheminImageJoueur;
        private string strCheminImageAdversaire;

        //Représente le chemin des images associées aux couleurs
        private string strCheminImageNoir;
        private string strCheminImageBlanc;

        //Plateau de jeu
        private Tableau m_leTableau;

        //Objet permettant de lier le vecteur de pbo au Flow Layout Panel
        private BindingSource m_sourcePbo;

        private int m_intNbTours;

        private TimeSpan m_tTempsJeu;

        //Objet afin de stocker les infos de l'adversaire
        private Joueur m_leJoueurAdverse;
        private int index;

        public int Index
        {
            get { return index; }
            set { index = value; }
        }
        #endregion

        #region ACCESSEURS
        //Indique si le joueur commence au premier tour
        public bool JoueurCommence
        {
            get { return blnJoueurCommence; }
            set { blnJoueurCommence = value; }
        }

        public Joueur leJoueurAdverse
        {
            get { return m_leJoueurAdverse; }
            set { m_leJoueurAdverse = value; }
        }
        public IA_Othello AI
        {
            get { return m_AI; }
            set { m_AI = value; }
        }

        public bool JoueurContreJouer
        {
            get { return blnJcJ; }
            set { blnJcJ = value; }
        }

        public Joueur leJoueur
        {
            get { return m_leJoueur; }
            set { m_leJoueur = value; }
        }

        public PictureBox PboSel
        {
            get { return this.m_pboSel; }
            set { this.m_pboSel = value; }
        }

        public String CheminImageJoueur
        {
            set { this.strCheminImageJoueur = value; }
        }
        public String CheminImageAdversaire
        {
            set { this.strCheminImageAdversaire = value; }
        }

        public Tableau LeTableau
        {
            get { return m_leTableau; }
            set { m_leTableau = value; }
        }        

        public int NbTours
        {
            get { return m_intNbTours; }
            set { m_intNbTours = value; }
        }

        public TimeSpan TempsJeu
        {
            get { return m_tTempsJeu; }
            set { m_tTempsJeu = value; }
        }
        #endregion

        public frmJeu()
        {
            InitializeComponent();
        }

        //TODO : renommer le bouton
        private void Button1_Click(object sender, EventArgs e)
        {
            frmRecords frmRecords1 = new frmRecords();

            if (frmRecords1.ShowDialog() == DialogResult.OK)
            {
                frmRecords1.Dispose();
            }
        }

        private void BtnQuitter_Click(object sender, EventArgs e)
        {
            EnregistrementRecords();
            Application.Exit();
        }

        private void frmJeu_Load(object sender, EventArgs e)
        {
            //L'image du joueur devient l'image associée à sa couleur
            if (leJoueur.Couleur == Tableau.NOIR)
            {
                //Si le Joueur = NOIR, l'image qu'il a choisi devient l'image
                //des jetons noirs et l'image de l'adversaire devient l'image
                //des jetons blancs
                strCheminImageNoir = strCheminImageJoueur;
                strCheminImageBlanc = strCheminImageAdversaire;
            }
            else
            {
                //Si le Joueur = BLANC, l'image qu'il a choisi devient l'image
                //des jetons blancs et l'image de l'adversaire devient l'image des
                //jetons noirs
                strCheminImageNoir = strCheminImageAdversaire;
                strCheminImageBlanc =  strCheminImageJoueur;
            }

            //Initialisation des noms et des images des joueurs
            pboImageJoueur.ImageLocation = strCheminImageJoueur;
            pboImageAdv.ImageLocation = strCheminImageAdversaire;
            pboTourDe.ImageLocation = strCheminImageNoir;
            txtNomJoueur.Text = leJoueur.Nom;
            txtNomAdversaire.Text = leJoueurAdverse.Nom;

            Tableau leTableau = new Tableau();
            LeTableau = leTableau;
            LeTableau.NouvellePartie(leJoueur.Couleur);

            //Initialisation du vecteur de pbo
            vectLesPbo = new PictureBox[8, 8];

            //Pour l'initialisation du Tag de chaque pbo           

            for (int y = 0; y < LeTableau.NbLignes; y++)
            {
                for (int x = 0; x < LeTableau.NbColonnes; x++)
                {
                    //Création du vecteur contenu dans le Tag des pbo
                    int[] leTag = new int[3];
                    leTag[0] = Tableau.VIDE;
                    leTag[1] = y;
                    leTag[2] = x;

                    vectLesPbo[y, x] = new PictureBox();
                    vectLesPbo[y, x].BackColor = Color.Green;
                    vectLesPbo[y, x].Size = new Size(79, 79);
                    vectLesPbo[y, x].SizeMode = PictureBoxSizeMode.StretchImage;
                    vectLesPbo[y, x].Tag = leTag;
                    vectLesPbo[y, x].Click += pbo_ClickLegal;
                    flpPbo.Controls.Add(vectLesPbo[y, x]);
                }
            }

            //Initialisation des bool qui détectent si la partie est terminée.
            blnCoupsJouablesJoueur = true;
            blnCoupsJouablesAdv = true;
            blnCoupJouable = true;
            
            MiseAJourTableau();

            CommencerTours();

            tmrTemps.Start();
            txtTempsRestant.Text = 60.ToString();
            m_tTempsJeu = TimeSpan.FromMinutes(60);
        }

        private void MiseAJourTableau()
        {
            for (int y = 0; y < LeTableau.NbLignes; y++)
            {
                for (int x = 0; x < LeTableau.NbColonnes; x++)
                {
                    int couleur = LeTableau[y, x];
                    int[] nouvTag = new int[3];
                    nouvTag[1] = y;
                    nouvTag[2] = x;
                    

                    if (couleur == Tableau.NOIR)
                    {
                        vectLesPbo[y, x].ImageLocation = strCheminImageNoir;
                        nouvTag[0] = Tableau.NOIR;
                        vectLesPbo[y, x].Tag = nouvTag;
                    }
                    else if (couleur == Tableau.BLANC)
                    {
                        vectLesPbo[y, x].ImageLocation = strCheminImageBlanc;
                        nouvTag[0] = Tableau.BLANC;
                        vectLesPbo[y, x].Tag = nouvTag;
                    }
                }
            }
            //Mise à jour de l'attribut NbJetons des objets Joueurs          
            if (leJoueur.Couleur == -1)
            {
                leJoueur.NbJetons = Convert.ToByte(LeTableau.NbJetonsNoirs);
                leJoueurAdverse.NbJetons = Convert.ToByte(LeTableau.NbJetonsBlancs);
            }
            else if (leJoueur.Couleur == 1)
            {
                leJoueur.NbJetons = Convert.ToByte(LeTableau.NbJetonsBlancs);
                leJoueurAdverse.NbJetons = Convert.ToByte(LeTableau.NbJetonsNoirs);
            }

            //Ajout par Laura Mise à jour des txtbox de l'interface
            txtVosPions.Text = Convert.ToString(leJoueur.NbJetons);
            txtPionsAdversaire.Text = Convert.ToString(leJoueurAdverse.NbJetons);
            txtCasesVides.Text = Convert.ToString(64 - LeTableau.NbJetonsPlaces);

        }

        private int CouleurJouee()
        {
            if(blnJoueurCommence)
            {
                if (NbTours % 2 == 0)
                {
                    return Tableau.BLANC;
                }
                else
                {
                    return Tableau.NOIR;
                }
            }
            else
            {
                if (NbTours % 2 == 0)
                {
                    return Tableau.NOIR;
                }
                else
                {
                    return Tableau.BLANC;
                }
            }

            
        }

        private void CommencerTours()
        {
            //La partie se termine si toutes les cases sont pleines et si aucun des deux joueurs n'a pu placer de jeton durant le
            //dernier tour
            if (LeTableau.NbJetonsPlaces == 64 || !blnCoupsJouablesAdv && !blnCoupsJouablesJoueur || LeTableau.NbJetonsBlancs == 0 || LeTableau.NbJetonsNoirs == 0)
                FinirPartie();
            else
            {
                //Le premier tour commence à 1, donc Noir joue en premier
                NbTours++;

                //Si c'est le tour du joueur
                if(CouleurJouee() == leJoueur.Couleur)
                {
                    blnCoupsJouablesJoueur = LeTableau.NouveauTour(CouleurJouee());
                    blnCoupJouable = blnCoupsJouablesJoueur;
                    txtTourDe.Text = leJoueur.Nom;
                    pboTourDe.ImageLocation = strCheminImageJoueur;
                }
                else
                {
                    blnCoupsJouablesAdv = LeTableau.NouveauTour(CouleurJouee());
                    blnCoupJouable = blnCoupsJouablesAdv;
                    txtTourDe.Text = leJoueurAdverse.Nom;
                    pboTourDe.ImageLocation = strCheminImageAdversaire;
                }
              
                if (!blnCoupJouable)
                {
                    MessageBox.Show(txtTourDe.Text + " passe son tour", "Aucun mouvement légal", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CommencerTours();
                }
                else
                {
                    AfficherCoupsValides();

                    //L'IA joue après tout la validation.
                    if(CouleurJouee() != leJoueur.Couleur && !blnJcJ)
                    {
                        tourIA();
                    }
                }

              
            }
        }

        private void tourIA()
        {
            Coordonnee coupIA = AI.TrouverMeilleurPosition(LeTableau, (int)AI.Difficulte, LeTableau.ConverserionCoupIA());
            LeTableau.PlacerJeton(coupIA.Y, coupIA.X, leJoueurAdverse.Couleur);
            System.Threading.Thread.Sleep(250);
            FinDeTour(coupIA.Y, coupIA.X);
        }

        private void FinDeTour(int posY, int posX)
        {
            MiseAJourTableau();

            string lettre;

            if (posY == 0)
            {
                lettre = "A";
            }
            else if (posY == 1)
            {
                lettre = "B";
            }
            else if (posY == 2)
            {
                lettre = "C";
            }
            else if (posY == 3)
            {
                lettre = "D";
            }
            else if (posY == 4)
            {
                lettre = "E";
            }
            else if (posY == 5)
            {
                lettre = "F";
            }
            else if (posY == 6)
            {
                lettre = "G";
            }
            else
            {
                lettre = "H";
            }

            string nomJoueur;
            if (CouleurJouee() == leJoueur.Couleur)
                nomJoueur = leJoueur.Nom;
            else
                nomJoueur = leJoueurAdverse.Nom;

            this.lstCoupJoue.SelectedIndex = this.lstCoupJoue.Items.Add(string.Format("{0} a joué: {1}, {2}", nomJoueur, posX+1, lettre));

            CommencerTours();
        }

        private void pbo_ClickLegal(object sender, EventArgs e)
        {
            PboSel = (PictureBox)sender;
            int[] tagTempo = (int[])PboSel.Tag;

            LeTableau.PlacerJeton(tagTempo[1], tagTempo[2], CouleurJouee());

            FinDeTour(tagTempo[1], tagTempo[2]);
        }

        private void frmJeu_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            
            Application.Exit();
        }

        /// <summary>
        /// Fait afficher les coups valides du tableau
        /// </summary>
        private void AfficherCoupsValides()
        {
            for (int y = 0; y < LeTableau.NbLignes; y++)
            {
                for (int x = 0; x < LeTableau.NbColonnes; x++)
                {
                    if (LeTableau.PlacementPossible[y, x])
                    {
                        vectLesPbo[y, x].BackColor = Color.LightGreen;
                        vectLesPbo[y, x].Enabled = true;
                    }
                    else
                    {
                        vectLesPbo[y, x].Enabled = false;
                        vectLesPbo[y, x].BackColor = Color.Green;
                    }
                }
            }
        }

        /// <summary>
        /// Méthode qui effectue le compteur de temps restant à l'utilisateur
        /// pour jouer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TempsCumule()
        {
            m_tTempsJeu = m_tTempsJeu.Subtract(TimeSpan.FromSeconds(1));

            txtTempsRestant.Text = m_tTempsJeu.Minutes + ":" + m_tTempsJeu.Seconds;
        }

        public void tmrTemps_Tick(object sender, EventArgs e)
        {
            TempsCumule();
        }



        /// <summary>
        /// La méthode est appelée lorsque la partie est terminée. Elle détermine
        /// s'il y a un gagnant ou non.
        /// </summary>
        private void FinirPartie()
        {
            //Arrêt du chronomètre
            tmrTemps.Stop();

            if (leJoueur.NbJetons > leJoueurAdverse.NbJetons)
            {
                MessageBox.Show(leJoueur.Nom + " a gagné!", "Fin de la partie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (leJoueur.NbJetons < leJoueurAdverse.NbJetons)
            {
                MessageBox.Show(leJoueurAdverse.Nom + " a gagné", "Fin de la partie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                MessageBox.Show("Partie nulle", "Partie nulle", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            //Mettre toutes les cases à enabled = false pour empêcher l'utilisateur de cliquer dessus
            for (int y = 0; y < LeTableau.NbLignes; y++)
            {
                for (int x = 0; x < LeTableau.NbColonnes; x++)
                {
                        vectLesPbo[y, x].Enabled = false;                                        
                }
            }
        }

        private void EnregistrementRecords()
        {
            StreamWriter fiche_records = new StreamWriter("C:\\images\\fiche_records.txt", true);

            fiche_records.WriteLine(m_leJoueur.Nom + ";" + m_leJoueur.Couleur + ";" + txtVosPions.Text);

            fiche_records.Close();
        }

        /// <summary>
        /// Ajout des résultats de la partie lors de la fermeture de l'application
        /// dans la fiche records.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmJeu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnRecommencer_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}

