﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using othello;



namespace othello
{
    [TestClass]
    public class TestsClasseTableau
    {
        //Constantes
        public const int NOIR = -1;
        public const int BLANC = 1;
        public const int VIDE = 0;

        [TestMethod]
        public void TestTableau()
        {
            Tableau unTableau = new Tableau();

            //Vérification si cases initialisées à vide
            Assert.AreEqual(unTableau.NbCases, 64);

            for (int y = 0; y < 8; y++)
            {
                for (int x = 0; x < 8; x++)
                {
                    Assert.AreEqual(unTableau[y, x], VIDE);
                }
            }

            //unTableau.NouvellePartie();
            Assert.AreEqual(unTableau[3, 3], BLANC);
            Assert.AreEqual(unTableau[4, 4], BLANC);
            Assert.AreEqual(unTableau[3, 4], NOIR);
            Assert.AreEqual(unTableau[4, 3], NOIR);
            


        }

        //public void TestsFrmRecords()
        //{
        //    StreamReader ficLectureRecords = new StreamReader("./fiche_records.txt");

        //    try
        //    {
        //        Assert.IsNotNull(ficLectureRecords);
        //    }

        //}

        public void TestsImages()
        {
            frmJeu formJeu = new frmJeu();

            // Si les images existent           
            //Assert.IsNotNull(formJeu.CheminImageNoir);
            //Assert.IsNotNull(formJeu.CheminImageBlanc);
           
               
           
        }

        public void TestsIA()
        {
            //  Tester l'IA
        }

        public void TestsJoueur()
        {
            //Tester si les joueurs ont une couleur différente pendant tout le jeu

            Joueur unJoueur = new Joueur("Patrick", -1);

            Assert.AreEqual(unJoueur.Nom, "Patrick");
            Assert.AreEqual(unJoueur.Couleur, -1);
            Assert.AreEqual(unJoueur.NbJetons, 2);

            unJoueur = new Joueur("Pierre", 1);

            Assert.AreEqual(unJoueur.Nom, "Pierre");
            Assert.AreEqual(unJoueur.Couleur, 1);
            Assert.AreEqual(unJoueur.NbJetons, 2);

            try
            {
                unJoueur = new Joueur("Marie",1);
                unJoueur.NbJetons = 70;
                Assert.Fail();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Assert.AreEqual(ex.Message, "Le nombre de jetons est en dehors de la plage permise.");
            }
        }

        /*TODO : Tests à faire
      
         Quand on place un jeton (accepte les bons coups,refuse les mauvais, retourne les jetons)       
         Tester lorsque le plateau est plein
         Si le comptage de points fonctionne (addition et soustraction)
         Comptage de temps        
             
             */
        [TestMethod]
        public void TestPlacerJeton()
        {

        }
    }
}
